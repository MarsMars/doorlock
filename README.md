# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration - put it in `config.json` file in the main directory
```json
{
  "network_SSID": "SID",
  "network_key": "KEY"
}
```  
* Dependencies
    * Logging module
```python
>>> import upip
>>> upip.install("logging")
```
* Database configuration
* How to run tests
* Deployment instructions
* Interaction with files with USB
```commandline
ampy --port /dev/ttyUSB0 get "/server client echo.py" tymek.py
ampy --port /dev/ttyUSB0 ls
ls *.py *.json | xargs -n 1 ampy --port /dev/ttyUSB0 put
```
* Connecting to board with USB
```commandline
picocom /dev/ttyUSB0 -b115200
```
To exit `picocom`  press `CRTL`+`a`+`x`
* Flashing micropython 
```commandline
esptool.py --chip esp32 --port /dev/ttyUSB0 write_flash -z 0x1000 esp32-20210418-v1.15.bin
```

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact