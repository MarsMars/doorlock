from machine import Pin
import _thread
import time
import wifi
import logging

ground = Pin(4, Pin.OUT)
red_led = Pin(5, Pin.OUT)
blue_led = Pin(16, Pin.OUT)
green_led = Pin(17, Pin.OUT)
red_led.off()
blue_led.off()
green_led.off()
ground.off()

wifi_led_status = False
log = logging.getLogger('log')


def start_wifi_thread():
    _thread.start_new_thread(wifi_thread, ())
    log.info("WiFi LED thread started")


def wifi_thread():
    while True:
        set_wifi_led()
        time.sleep_ms(500)


def set_wifi_led():
    if wifi.gateway.isconnected():
        blink_wifi_led()
    else:
        red_led.off()
        log.warning("WiFi disconnected")


def blink_wifi_led():
    global wifi_led_status
    if not wifi_led_status:
        blue_led.on()
        wifi_led_status = True
        log.debug("WiFi LED on")
    else:
        blue_led.off()
        wifi_led_status = False
        log.debug("WiFi LED off")
