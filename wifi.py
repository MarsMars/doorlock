from config import config
import network
import logging

gateway = network.WLAN(network.STA_IF)
log = logging.getLogger('log')


def connect():
    gateway.active(True)
    gateway.connect(config['network_SSID'], config['network_key'])
    while not gateway.isconnected():
        pass
    log.info("WiFi connected to %s" % config['network_SSID'])
    log.info("IP address: %s" % gateway.ifconfig()[0])
