import logging
import bluetooth

log = logging.getLogger('log')

_IRQ_CENTRAL_CONNECT = 1
_IRQ_CENTRAL_DISCONNECT = 2
_IRQ_GATTS_WRITE = 3
_IRQ_GATTS_READ_REQUEST = 4
_IRQ_SCAN_RESULT = 5
_IRQ_SCAN_DONE = 6
_IRQ_PERIPHERAL_CONNECT = 7
_IRQ_PERIPHERAL_DISCONNECT = 8
_IRQ_GATTC_SERVICE_RESULT = 9
_IRQ_GATTC_SERVICE_DONE = 10
_IRQ_GATTC_CHARACTERISTIC_RESULT = 11
_IRQ_GATTC_CHARACTERISTIC_DONE = 12
_IRQ_GATTC_DESCRIPTOR_RESULT = 13
_IRQ_GATTC_DESCRIPTOR_DONE = 14
_IRQ_GATTC_READ_RESULT = 15
_IRQ_GATTC_READ_DONE = 16
_IRQ_GATTC_WRITE_DONE = 17
_IRQ_GATTC_NOTIFY = 18
_IRQ_GATTC_INDICATE = 19

_ADV_TYPE_FLAGS = 0x01
_ADV_TYPE_NAME = 0x09
_ADV_TYPE_UUID16_COMPLETE = 0x3
_ADV_TYPE_UUID32_COMPLETE = 0x5
_ADV_TYPE_UUID128_COMPLETE = 0x7
_ADV_TYPE_UUID16_MORE = 0x2
_ADV_TYPE_UUID32_MORE = 0x4
_ADV_TYPE_UUID128_MORE = 0x6
_ADV_TYPE_APPEARANCE = 0x19


def decode_field(payload, adv_type):
    i = 0
    result = []
    while i + 1 < len(payload):
        log.info("i={}, size={}, type={}".format(i, payload[i], payload[i+1]))
        if payload[i + 1] in (adv_type, 0x8):
            log.info("type={}".format(adv_type))
            result.append(payload[i + 2 : i + payload[i] + 1])
        i += 1 + payload[i]
    return result


def decode_name(payload):
    n = decode_field(payload, _ADV_TYPE_NAME)
    return str(n[0], "utf-8") if n else ""


class Mobile(object):
    def __init__(self):
        # self._address = 'B4:1A:1D:16:4B:E2'
        self._addr = None
        self._addr_type = None
        self._name = None
        self._ble = bluetooth.BLE()
        self._ble.active(True)
        self._ble.irq(self._irq)
        self._data = []
        log.info("Bluetooth object created")

    def _irq(self, event, data):
        # log.info(event)
        addr_type, addr, adv_type, rssi, adv_data = data
        self._addr_type = addr_type
        self._addr = bytes(addr)
        self._name = decode_name(adv_data) or "?"
        self._data.append(adv_data)
        # log.info(self._addr_type)
        # log.info(self._addr)
        # log.info(rssi)
        # log.info(self._name)
        # log.info(adv_data)
        log.info("Bluetooth scan finished")

    def scan(self):
        log.info("Bluetooth scan started")
        self._ble.gap_scan(2000, 30000, 30000)

    def find_devices(self):
        print('Finding BT devices')
        nearby_devices = bluetooth.discover_devices(duration=8, lookup_names=True,
                                                    flush_cache=True, lookup_class=False)
        print('Found %s devices', len(nearby_devices))
        for address, name in nearby_devices:
            print('address: %s, name: %s', address, name)

    def connect(self):
        print('Connecting to Galaxy S20')
        return self._socket.connect((self._address, self._addr_type))

    def close(self):
        print('Closing connection to Galaxy S20')
        self._socket.close()

    def get_rssi(self):
        print('Reading RSSI from Galaxy S20')
