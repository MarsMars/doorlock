from unittest import TestCase
from unittest.mock import patch
from mobile import Mobile

#
# class TestMobile(TestCase):
#     def setUp(self) -> None:
#         self._mb = Mobile()
#
#     @patch('bluetooth.discover_devices')
#     def test_find_devices(self, mock_discover_devices):
#         mock_discover_devices.return_value = [('A', 'N')]
#         self._mb.find_devices()
#         mock_discover_devices.assert_called_once()
#
#     @patch('bluetooth.BluetoothSocket.connect')
#     @patch('bluetooth.BluetoothSocket.close')
#     def test_connect_and_close(self, mock_connect, mock_close):
#         self._mb.connect()
#         self._mb.close()
#         mock_connect.assert_called_once()
#         mock_close.assert_called_once()
#
#     def test_get_rssi(self):
#         self._mb.get_rssi()
#         self.fail()
