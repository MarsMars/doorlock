import unittest


class TestConfig(unittest.TestCase):
    def test_network_configuration(self):
        from config import config
        self.assertIsNotNone(config['network_SSID'])
        self.assertIsNotNone(config['network_key'])


if __name__ == '__main__':
    unittest.main()
