import unittest
from unittest.mock import patch
import wifi


class TestWiFi(unittest.TestCase):
    @patch('network.WLAN.active')
    @patch('network.WLAN.connect')
    @patch('network.WLAN.isconnected')
    def test_connection(self, mock_isconnected, mock_connect, mock_active):
        wifi.connect()
        mock_active.assert_called_once()
        mock_connect.assert_called_once()
        mock_isconnected.assert_called()


if __name__ == '__main__':
    unittest.main()
