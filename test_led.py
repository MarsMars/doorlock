import unittest
from unittest.mock import patch
import led


class TestLed(unittest.TestCase):
    @patch('network.WLAN.isconnected')
    @patch('machine.Pin.on')
    def test_network_led_on(self, mock_pin_on, mock_isconnected):
        mock_isconnected.return_value = True
        led.set_wifi_led()
        mock_pin_on.assert_called_once()

    @patch('network.WLAN.isconnected')
    @patch('machine.Pin.off')
    def test_network_led_off(self, mock_pin_off, mock_isconnected):
        mock_isconnected.return_value = False
        led.set_wifi_led()
        mock_pin_off.assert_called_once()

    @patch('machine.Pin.on')
    @patch('machine.Pin.off')
    def test_blink_wifi_led(self, mock_pin_off, mock_pin_on):
        led.blink_wifi_led()
        led.blink_wifi_led()
        mock_pin_on.assert_called_once()
        mock_pin_off.assert_called_once()

    @patch('_thread.start_new_thread')
    def test_start_led_thread(self, mock_start_thread):
        led.start_wifi_thread()
        mock_start_thread.assert_called_once_with(led.wifi_thread, ())


if __name__ == '__main__':
    unittest.main()
