import unittest
from unittest.mock import patch
from mobile import Mobile


class TestMobile(unittest.TestCase):
    def setUp(self) -> None:
        super().setUp()
        self.mb = Mobile()

    def test_connect(self):
        self.mb.connect()

    @patch('bluetooth.ble.DiscoveryService')
    @patch('bluetooth.ble.DiscoveryService.discover')
    def test_scan(self, mock_discover, mock_DiscoveryService):
        self.mb.scan()
        mock_discover.assert_called_once()


if __name__ == '__main__':
    unittest.main()
